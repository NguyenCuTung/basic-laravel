@extends('layouts.app')

@section('content')

    <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
        <div class="panel panel-body">
            <div class="col-md-9 col-lg-9 col-sm-9 pull-left">
                <div class="row col-md-12 col-lg-12 col-sm-12" style="background: white; margin: 10px;">
                    <h1>Create New Child Category</h1>
                    <form action="{{ route('childCs.store') }}" method="post">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="childC-name">Name <span class="text-danger">*</span></label>
                            <input placeholder="Enter Name"
                                   id="childC-name"
                                   required
                                   name="name"
                                   spellcheck="false"
                                   class="form-control"
                            >
                        </div>
                        <div class="form-group">
                            <label for="parent-name">Parent Name <span class="text-danger">*</span></label>
                            <select  class="form-control" name="parent_name" id="parent-name" required spellcheck="false" >
                                @foreach($parentC as $value)

                                    <option value="{{ $value->id }}">{{ $value->name }}</option>

                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary pull-right" value="submit">
                        </div>
                    </form>

                </div>
            </div>

            <div class="col-sm-3 col-md-3 col-lg-3 pull-right">
                <div class="sidebar-module">
                    <h4>Action</h4>
                    <ol class="list-unstyled">
                        <li><a href="/childCs">My Childs Category</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    @endsection