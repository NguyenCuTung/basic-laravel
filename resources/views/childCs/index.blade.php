@extends('layouts.app')

@section('content')

    <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
        <div class="panel panel-primary">
            <div class="panel-heading">Child Category <a href="/childCs/create" class="pull-right btn btn-primary btn-xs">Create New</a></div>
            <div class="panel-body">
                <table class="table table-bordered table-auto">
                    <thead class="panel-title">
                        <th class="text-center">Child Category Name</th>
                        <th class="text-center">Parent Category Name</th>
                        <th class="text-center">Detail</th>
                    </thead>
                    <tbody>

                    @foreach($childCs as $childC)

                        <tr>
                            <td class="text-center">{{ $childC->name }}</td>
                            <td class="text-center">{{ $childC->getParent->name }}</td>
                            <td class="text-center"><a href="/childCs/{{ $childC->id }}" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-info-sign "></span></a></td>
                        </tr>

                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @endsection