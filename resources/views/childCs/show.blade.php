@extends('layouts.app')



@section('content')

    <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
        <div class="panel panel-primary">
            <div class="panel-body">
                <div class="col-md-9 col-lg-9 col-sm-9 pull-left">
                    <div class="col-md-10 col-lg-10 col-sm-10 pull-left">
                        <div class="jumbotron">
                            <table class="table table-borderedb table-condensed">
                                <thead class="panel panel-primary panel-title">
                                    <th class="text-center">Parent Category</th>
                                    <th class="text-center">Child Category</th>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td class="text-center">{{ $childC->name }}</td>
                                        <td class="text-center">{{ $childC->getParent->name }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-2 col-lg-2 col-sm-2 pull-right">
                        <div class="sidebar-module">
                            <h4>Action</h4>
                            <ol class="list-unstyled">
                                <li><a href="/childCs/{{ $childC->id }}/edit">Edit</a></li>
                                <br>
                                <li><a href="#"
                                       onclick="
                                            var result = confirm('Are you sure you wish to delete this child category?');
                                            if (result)
                                                {
                                                    event.preventDefault();
                                                    document.getElementById('delete-form').submit();
                                                }
                                       "
                                    >Delete</a>
                                    <form id="delete-form" action="{{ route('childCs.destroy', [$childC->id]) }}" method="post" style="display: none">
                                        {{ method_field('DELETE')  }}
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection