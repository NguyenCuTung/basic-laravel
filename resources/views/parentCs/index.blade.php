@extends('layouts.app')

@section('content')

    <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
        <div class="panel panel-primary">
            <div class="panel-heading">Parent Category <a href="/parentCs/create" class="btn btn-primary btn-xs pull-right">Create New</a></div>
            <div class="panel-body">
                <table class="table table-bordered table-striped table-auto table-condensed">
                    <thead class="panel-title">
                        <th class="text-center">Parent Category Name</th>
                        <th class="text-center">Detail</th>
                    </thead>
                    <tbody>
                    
                    @foreach($parentCs as $parentC)
                        
                        <tr>
                            <td class="text-center">{{ $parentC->name }}</td>
                            <td class="text-center">
                                <a href="parentCs/{{ $parentC->id }}" class="btn btn-success btn-xs">
                                    <span class="glyphicon glyphicon-info-sign"></span>
                                </a>
                            </td>
                        </tr>
                        
                        @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @endsection