@extends('layouts.app')

@section('content')

    <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
        <div class="panel panel-primary">
            <div class="panel-body">
                <div class="col-md-9 col-lg-9 col-sm-9 pull-left">
                    <h1>Create New Parent Category</h1>
                    <form action="{{ route('parentCs.store') }}" method="post">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="parentC-name">Name <span class="text-danger">*</span></label>
                            <input placeholder="Enter name"
                                   id="parentC-name"
                                   required
                                   name="name"
                                   spellcheck="false"
                                   class="form-control"
                            >
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary pull-right" value="submit">
                        </div>
                    </form>
                </div>
                <div class="col-sn-3 col-md-3 col-lg-3 pull-right">
                    <div class="sidebar-module">
                        <h4>Action</h4>
                        <ol class="list-unstyled">
                            <li><a href="/parentCs">My Parent Categories</a></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection