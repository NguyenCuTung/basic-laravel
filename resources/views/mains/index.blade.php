@extends('layouts.app')

@section('content')
    {{-- Navigation --}}
    <nav class="navbar navbar-defauld">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="index.php" class="navbar-brand">Shop Online</a>
                <ul class="nav navbar-nav ">
                {{-- Menu --}}
                @foreach($parentC as $value)
                    <!--                    Menu -->
                        <li class="nav navbar-right">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ $value->name }}
                                <span class="caret"></span></a>
                        </li>
                    @endforeach
                    <li><a href=""><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
                </ul>
            </div>
        </div>
    </nav>


    <div id="headerWrapper">
        <div id="back-flower"></div>
        <div id="logotext"></div>
        <div id="fore-flower"></div>
    </div>

    <div class="row">
        {{-- left bar --}}
        <div class="col-md-2">

        </div>
        {{-- content --}}

        <div class="col-md-8"><h2 class="text-center" align="center">Feature Products</h2>

            <div class="row">
                @foreach($product as $value)
                    <div class="col-md-3 ">
                        <h4><?php echo $value['title'];?></h4>
                        <img src="<?php echo $value['image'];?>" alt="<?php echo $value['title'];?>" class="img-thumb" height="150" width="150"/>
                        <p class="list-price text-danger">List Price: <s>$<?php echo $value['list_price'];?></s></p>
                        <p class="price">Our Price: $<?php echo $value['price'];?></p>
                        <button type="button" class="btn btn-sm btn-success"
                                onclick="detailsmodal(<?= $value['id'];?>)">
                            Details
                        </button>
                    </div>
                @endforeach
            </div>
        </div>

        {{-- right bar --}}
        <div class="col-md-2">

        </div>
    </div>
    <hr>
    <footer class="text-center" id="footer">&copy; Copyright 2013-2017 Shop-Online</footer>

@endsection
