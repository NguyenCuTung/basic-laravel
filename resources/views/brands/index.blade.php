@extends('layouts.app')

@section('content')

    <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
        <div class="panel panel-primary ">
            <div class="panel-heading">Brand <a  class="pull-right btn btn-primary btn-xs" href="/brands/create">Create New</a></div>
            <div class="panel-body">
                <table class="table table-bordered table-striped table-auto table-condensed">
                    <thead class="panel-title">
                    <th class="text-center">Brand Name</th>
                    <th class="text-center">Detail</th>
                    </thead>
                    <tbody>

                    @foreach($brands as $brand)

                    <tr>
                        <td class="text-center">{{ $brand->name }}</td>
                        <td class="text-center"><a href="/brands/{{ $brand->id }}" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-info-sign "></span></a></td>
                    </tr>

                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection