@extends('layouts.app')

@section('content')


    <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
        <div class="panel panel-primary ">
            <div class="panel-body">
                <div class="col-md-9 col-lg-9 col-sm-9 pull-left">
                    <div class="col-md-9 col-lg-9 col-sm-9 pull-left">

                        <div class="jumbotron">
                            <h1>{{ $brand->name }}</h1>
                        </div>

                    </div>

                    <div class="col-sm-3 col-md-3 col-lg-3 pull-right">
                        <div class="sidebar-module">
                            <h4>Action</h4>
                            <ol class="list-unstyled">
                                <li><a href="/brands/{{ $brand->id }}/edit">Edit</a></li>
                                <br>
                                <li>
                                    <a href="#"
                                       onclick="
                                            var result = confirm('Are you sure you wish to delete this Project?');
                                            if(result)
                                            {
                                            event.preventDefault();
                                            document.getElementById('delete-form').submit();
                                            }
                                            "
                                    >
                                        Delete
                                    </a>
                                    <form id="delete-form" action="{{ route('brands.destroy',[$brand->id]) }}"
                                          method="post" style="display: none;">
                                        <input type="hidden" name="_method" value="delete">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ol>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection