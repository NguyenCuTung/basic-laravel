@extends('layouts.app')

@section('content')

    <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3">
        <div class="panel panel-primary ">
            <div class="panel-body">
                <div class="col-md-9 col-lg-9 col-sm-9 pull-left">

                    <div class="row col-md-12 col-lg-12 col-sm-12" style="background: white; margin: 10px">
                        <h1>Create New Brand</h1>
                        <form action="{{ route('brands.store') }}" method="post">
                            {{ csrf_field() }}


                            <div class="form-group">
                                <lable for="brand-name">Name <span class="text-danger">*</span></lable>
                                <input placeholder="Enter name"
                                       id="brand-name"
                                       required
                                       name="name"
                                       spellcheck="false"
                                       class="form-control"
                                >
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary pull-right" value="submit">
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-sm-3 col-md-3 col-lg-3 pull-right">
                    <div class="sidebar-module">
                        <h4>Action</h4>
                        <ol class="list-unstyled">
                            <li><a href="/brands">My Brands</a></li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection