@extends('layouts.app')

@section('content')

    <div class="col-md-12 col-lg-12 ">
        <div class="panel panel-primary">
            <div class="panel-heading">Product <a href="/products/create" class="pull-right btn btn-primary btn-xs">Create New</a></div>
            <div class="panel-body">
                <table class="table table-bordered table-auto">
                    <thead class="panel-title">
                        <th class="text-center">Title</th>
                        <th class="text-center">Price</th>
                        <th class="text-center">List-Price</th>
                        <th class="text-center">Brand</th>
                        <th class="text-center">Parent Category</th>
                        <th class="text-center">Child Category</th>
                        <th class="text-center">Image</th>
                        <th class="text-center">Description</th>
                        <th class="text-center">Sizes</th>
                        <th class="text-center">Detail</th>
                    </thead>

                    <tbody>

                    @foreach($products as $product)

                        <tr>
                            <td class="text-center">{{ $product->title }}</td>
                            <td class="text-center">{{ $product->price }}</td>
                            <td class="text-center">{{ $product->list_price }}</td>
                            <td class="text-center">{{ $product->getBrand->name }}</td>
                            <td class="text-center">{{ $product->getParent->name }}</td>
                            <td class="text-center">{{ $product->getChild->name }}</td>
                            <td class="text-center"><img src="{{ $product->image }}" alt="" class="img-thumbnail" width="120px" height="120px"></td>
                            <td class="text-center">{{ $product->description }}</td>
                            <td class="text-center">{{ $product->sizes }}</td>
                            <td class="text-center"><a href="/products/{{ $product->id }}" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-info-sign"></span></a></td>
                        </tr>

                        @endforeach

                    </tbody>

                </table>
            </div>
        </div>
    </div>

    @endsection