@extends('layouts.app')

@section('content')

    <div class="col-md-12 col-lg-12 col">
        <div class="panel panel-primary">
            <div class="panel-body">
                <div class="col-md-10 col-lg-10 col-sm-10 pull-left">
                    <div class="jumbotron">
                        <table class="table table-bordered ">
                            <thead class="panel panel-primary panel-title">
                                <th class="text-center">Title</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Brand</th>
                                <th class="text-center">Category</th>
                                <th class="text-center">Image</th>
                                <th class="text-center">Description</th>
                                <th class="text-center">Sizes</th>
                            </thead>

                            <body class="">
                                <tr>
                                    <td class="text-center">{{ $product->title }}</td>
                                    <td class="text-center">{{ $product->price }}</td>
                                    <td class="text-center">{{ $product->getBrand->name }}</td>
                                    <td class="text-center">{{ $product->getParent->name }} <span class="text-danger">~</span> {{ $product->getChild->name }}</td>
                                    <td class="text-center"><img src="http://127.0.0.1:8000/{{ $product->image }}" alt="" class="img-thumbnail" width="120px" height="120px"></td>
                                    <td class="text-center">{{ $product->description }}</td>
                                    <td class="text-center">{{ $product->sizes }}</td>
                                </tr>
                            </body>
                        </table>
                    </div>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 pull-right">
                    <div class="sidebar-module">
                        <h4>Action</h4>
                        <ol class="list-unstyled">
                            <li><a href="/products/{{ $product->id }}/edit">Edit</a></li>
                            <br>
                            <li><a href="#"
                                   onclick="
                                            var result = confirm('Are you sure you wish to delete this Product?');
                                            if (result)
                                                {
                                                    event.preventDefault();
                                                    document.getElementById('delete-form').submit();
                                                }
                                       "
                                >Delete</a>
                                <form id="delete-form" action="{{ route('products.destroy', [$product->id]) }}" method="post" style="display: none">
                                    {{ method_field('DELETE')  }}
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection