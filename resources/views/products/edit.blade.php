@extends('layouts.app')

@section('content')

    <h1><center>Edit Product</center></h1>
    <hr>
    <form action="{{ route('products.update', $product->id) }}" method="post">
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <div class="form-group col-md-3" >
            <label for="title">Title <span class="text-danger">*</span></label>
            <input type="text"
                   id="title"
                   required
                   name="title"
                   spellcheck="false"
                   class="form-control"
                   value="{{ $product->title }}"
            >
        </div>
        <div class="form-group col-md-3">
            <label for="brand">Brand <span class="text-danger">*</span></label>
            <select name="brand" id="brand" class="form-control">
                @foreach($brand as $value)

                    <option value="{{ $value->id }}">{{ $value->name }}</option>

                    @endforeach
            </select>
        </div>
        <div class="form-group col-md-3">
            <label for="parent_name">Parent Category <span class="text-danger">*</span></label>
            <select name="parentC" id="parentC" class="form-control">
                @foreach($parentC as $value)

                    <option value="{{ $value->id }}">{{ $value->name }}</option>

                    @endforeach
            </select>
        </div>
        <div class="form-group col-md-3">
            <label for="child_name">Child Category <span class="text-danger">*</span></label>
            <select name="childC" id="child" class="form-control">
                @foreach($childC as $value)

                    <option value="{{ $value->id }}">{{ $value->name }}</option>

                    @endforeach
            </select>
        </div>
        <div class="form-group col-md-3">
            <label for="price">Price <span class="text-danger">*</span></label>
            <input type="text"
                   id="price"
                   required
                   name="price"
                   spellcheck="false"
                   class="form-control"
                   value="{{ $product->price }}"
            >
        </div>
        <div class="form-group col-md-3">
            <label for="list_price">List Price <span class="text-danger">*</span></label>
            <input type="text"
                   id="list_price"
                   required
                   name="list_price"
                   spellcheck="false"
                   class="form-control"
                   value="{{ $product->list_price }}"
            >
        </div>
        <div class="form-group col-md-3">
            <label for="">Quantity & Sizes <span class="text-danger">*</span></label>
            <button class="btn btn-default form-control" onclick="jQuery('#sizesModal').modal('toggle'); return false;">Quantity & Size</button>
        </div>
        <div class="form-group col-md-3">
            <label for="sizes">Size & Qty Preview</label>
            <input type="text" name="sizes" class="form-control" value="{{ $product->sizes }}" id="sizes" required readonly>
        </div>
        <div class="form-group col-md-6">

        </div>
        <div class="form-group col-md-6">
            <label for="description">Description:</label>
            <textarea name="description" id="description" rows="6" class="form-control">{{ $product->description }}</textarea>
        </div>
        <br>
        <div class="form-group col-md-11">
            <input type="submit" class="btn btn-primary pull-right" value="submit">
        </div>
        <div class="clearfix"></div>
    </form>

    {{--Modal--}}

    <div class="modal fade" id="sizesModal" tabindex="-1" role="dialog" aria-labelledby="sizesModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dimiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="sizeModalLabel">Sizes & Quantity</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <?php for($i=1;$i<=12;$i++):?>
                        <div class="form-group col-md-4">
                            <label for="size<?=$i;?>">Size:</label>
                            <input type="text" name="size<?=$i;?>" id="size<?=$i;?>" value="<?=((!empty($sArray[$i -1]))?$sArray[$i-1]:'');?>" class="form-control">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="qty<?=$i;?>">Quantity:</label>
                            <input type="number" name="qty<?=$i;?>" id="qty<?=$i;?>" value="<?=((!empty($qArray[$i -1]))?$qArray[$i-1]:'');?>" min="0" class="form-control">
                        </div>
                        <?php endfor;?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="updateSizes(); jQuery('#sizesModal').modal('toggle');return false; ">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        function updateSizes() {
            var sizeString = '';
            for(var i=1;i<=12;i++){
                if(jQuery('#size'+i).val()!=''){
                    sizeString += jQuery('#size'+i).val()+':'+jQuery('#qty'+i).val()+',';
                }
            }
            jQuery('#sizes').val(sizeString);
        }
    </script>
@endsection