<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

//Route::get('home', function () {
//    return response('Hello World', 200)
//        ->header('Content-Type', 'text/plain');
//});
Route::get('brands', function (){

});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'MainController@index')->name('mains');
Route::middleware(['auth'])->group(function () {

    Route::resource('brands', 'BrandController');
    Route::resource('parentCs', 'ParentController');
    Route::resource('childCs', 'ChildController');
    Route::resource('products' , 'ProductController');

});