<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use Validator;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

            $brands = Brand::get();
            return view('brands.index', ['brands' => $brands]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $brand = Brand::create([
            'name' => $request->input('name')
        ]);
//            if ($brand){
//                return redirect()->route('brands.show', ['brand'=>$brand->id])->with('success', 'Brand create successfully');
//            }
        if ($brand) {
            return redirect('brands/' . $brand->id)->withErrors($brand)->withInput();
        }
    }

//
//    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        $brand = Brand::findOrFail($brand->id);

        return view('brands.show', ['brand'=>$brand]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        $brand = Brand::find($brand->id);

        return view('brands.edit',['brand'=>$brand]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        $brandUpdate = Brand::where('id', $brand->id)
            ->update([
                'name' => $request->input('name'),
            ]);
        if ($brandUpdate){
            return redirect()->route('brands.show', ['brand'=>$brand->id]);
        }
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        //
        $findBrand = Brand::find( $brand->id);

        if ($findBrand->delete()){
            return redirect()->route('brands.index')->with('success', 'Brand have id  deleted success');
        }

        return back()->withInput()->with('error' , 'Brand could not be deleted');
    }
}
