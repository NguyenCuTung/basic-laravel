<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ChildC;
use App\ParentC;
class ChildController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
        $childC = ChildC::all();
        return view('childCs.index', ['childCs'=> $childC]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parentC = ParentC::all();
        return view('childCs.create', compact( 'parentC'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $childC = ChildC::create([
            'name'=>$request->input('name'),
            'parent_name'=>$request->input('parent_name')
        ]);
        if ($childC){
            return redirect()->route('childCs.show', ['childC'=>$childC]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ChildC $childC)
    {
        //
        $childC = ChildC::find($childC->id);
        return view('childCs.show', ['childC' => $childC]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ChildC $childC)
    {
        //
        $parentC = ParentC::all();
        $childC = ChildC::find($childC->id);

        return view('childCs.edit', ['childC'=>$childC,'parentC'=>$parentC]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChildC $childC)
    {
        //
        $childCUpdate = ChildC::where('id',$childC->id)
            ->update([
                'name'=>$request->input('name'),
                'parent_name'=>$request->input('parent_name')
            ]);
        if ($childCUpdate){
            return redirect()->route('childCs.show', ['childC'=>$childC->id]);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChildC $childC)
    {
        //
        $findChildC = ChildC::find( $childC->id);

        if ($findChildC->delete()){
            return redirect()->route('childCs.index');
        }
    }
}
