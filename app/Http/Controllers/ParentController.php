<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ParentC;

class ParentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $parentCs = ParentC::get();

        return view('parentCs.index', ['parentCs' =>$parentCs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('parentCs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $parentC = ParentC::create([
           'name'=>$request->input('name')
        ]);
        if ($parentC){
            return redirect()->route('parentCs.show', ['parentC'=>$parentC->id]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ParentC $parentC)
    {
        //
        $parentC = ParentC::find($parentC->id);

        return view('parentCs.show',['parentC'=>$parentC]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ParentC $parentC)
    {
        //
        $parentC = ParentC::find($parentC->id);

        return view('parentCs.edit', ['parentC' =>$parentC]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ParentC $parentC)
    {
        $parentCupdate = ParentC::where('id', $parentC->id)
            ->update([
                'name' => $request->input('name'),
            ]);
        if ($parentCupdate){
            return redirect()->route('parentCs.show', ['parentC'=>$parentC->id]);
        }
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ParentC $parentC)
    {
        $findParentC = ParentC::find($parentC->id);

        if ($findParentC->delete()){
            return redirect()->route('parentCs.index');
        }
    }

}
