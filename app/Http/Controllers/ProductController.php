<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Product;
use App\Brand;
use App\ParentC;
use App\ChildC;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;
use Store;
use File;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $product = Product::get();
        return view('products.index', ['products'=>$product]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $brand = Brand::all();
        $parentC = ParentC::all();
        $childC = ChildC::all();
        return view('products.create', compact('brand','parentC','childC'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $user  = new file;
        $user ->title = Input::get('title');
        $user ->price = Input::get('price');
        $user ->list_price = Input::get('list_price');
        $user ->brand = Input::get('brand');
        $user ->parentC = Input::get('parentC');
        $user ->childC = Input::get('childC');
        $user ->description = Input::get('description');
        $user ->sizes = Input::get('sizes');
        if (Input::hasFile('image')){
            $file = Input::file('image');
            $file ->move(public_path().'/',$file->getClientOriginalName());
            $user->image = $file->getClientOriginalName();

        }
        $product = Product::create([
            'title'=>$user ->title,
            'price'=>$user ->price,
            'list_price'=>$user ->list_price,
            'brand'=>$user ->brand,
            'parentC'=>$user ->parentC,
            'childC'=>$user ->childC,
            'image'=> $user ->image,
            'description'=>$user ->description,
            'sizes'=>$user->sizes
        ]);
        if ($product){
            return redirect()->route('products.show', ['product'=>$product]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
        $product = Product::find($product->id);
        $image = Product::find($product->image);
        return view('products.show', ['product'=>$product,'image'=>$image]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
        $brand = Brand::all();
        $parentC = ParentC::all();
        $childC = ChildC::all();
        $product = Product::find($product->id);

        return view('products.edit', ['product'=>$product, 'parentC'=>$parentC, 'childC'=>$childC, 'brand'=>$brand]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
        $user  = new file;
        $user ->title = Input::get('title');
        $user ->price = Input::get('price');
        $user ->list_price = Input::get('list_price');
        $user ->brand = Input::get('brand');
        $user ->parentC = Input::get('parentC');
        $user ->childC = Input::get('childC');
        $user ->description = Input::get('description');
        $user ->sizes = Input::get('sizes');

        $product = Product::where('id', $product->id)
            ->update([
                'title'=>$user ->title,
                'price'=>$user ->price,
                'list_price'=>$user ->list_price,
                'brand'=>$user ->brand,
                'parentC'=>$user ->parentC,
                'childC'=>$user ->childC,
                'description'=>$user ->description,
                'sizes'=>$user->sizes
            ]);
        if ($product){
            return redirect()->route('products.show', ['product'=>$product]);
        }

//        $productUpdate = Product::where('id', $product->id)
//            ->update([
//                'title'=>$request->input('title'),
//                'price'=>$request->input('price'),
//                'list_price'=>$request->input('list_price'),
//                'brand'=>$request->input('brand'),
//                'parentC'=>$request->input('parentC'),
//                'childC'=>$request->input('childC'),
//                'image'=>$request->input('image'),
//                'description'=>$request->input('description'),
//                'sizes'=>$request->input('sizes')
//            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
        $findProduct = Product::find($product->id);
        if ($findProduct->delete()){
            return redirect() ->route('products.index');
        }
    }
}
