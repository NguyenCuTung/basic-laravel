<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    //
    use SoftDeletes;
    protected $table = 'products';

    protected $fillable = [
        'title',
        'price',
        'list_price',
        'brand',
        'parentC',
        'childC',
        'image',
        'description',
        'featured',
        'sizes',
        'deleted'
                            ];

    public function getBrand(){
        return $this->belongsTo('App\Brand', 'brand');
    }

    public function getParent(){
        return $this->belongsTo('App\ParentC', 'parentC');
    }

    public function getChild(){
        return $this->belongsTo('App\ChildC', 'childC');
    }
    protected $dates = ['deleted_at'];
}
