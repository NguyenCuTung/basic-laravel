<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChildC extends Model
{
    //
    use SoftDeletes;
    protected $table = 'childC';

    protected $fillable = [
        'name',
        'parent_name'
    ];

    public function getParent()
    {
        return $this->belongsTo('App\ParentC', 'parent_name');
    }
    protected $dates = ['deleted_at'];

}
