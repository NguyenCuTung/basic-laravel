<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParentC extends Model
{
    protected $table = 'parentC';
    use SoftDeletes;
    //
    protected $fillable = [
      'name'
    ];

    public function getChild()
    {
        return $this->hasMany('App\ChildC', 'parent_name');
    }

    public function getProduct()
    {
        return $this->hasMany('App\Product', 'parentC');
    }

    protected $dates = ['deleted_at'];
}
