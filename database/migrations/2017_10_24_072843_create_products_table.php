<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->decimal('price','10','2');
            $table->decimal('list_price','10','2');
            $table->integer('brand')->unsigned();
            $table->integer('parentC')->unsigned();
            $table->integer('childC')->unsigned();
            $table->string('image');
            $table->text('description')->nullable();
            $table->tinyInteger('featured')->nullable()->unsigned();
            $table->text('sizes');
            $table->tinyInteger('deleted')->nullable()->unsigned();

            $table->foreign('brand')->references('id')->on('brands')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('parentC')->references('id')->on('parentC')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('childC')->references('id')->on('childC')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
